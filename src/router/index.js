import Vue from 'vue'
import Router from 'vue-router'
// === PAGES ===
// My Courses
import MyCourses from '@/components/MyCourses'
import Courses from '@/components/MyCourses/Courses'
import Resources from '@/components/MyCourses/Resources'
// Reports
import Reports from '@/components/Reports/'
// User Profile
import UserProfileManagement from '@/components/UserProfileManagement'
// Manager User Profile Permission
import ManageUserPermission from '@/components/UserProfileManagement/ManageUserPermission'
// Manager Auth
import ManagerAuthorization from '@/components/ManagerAuthorization'
// Group Management
import GroupManagement from '@/components/GroupManagement'
import Roles from '@/components/GroupManagement/Roles'
import WorkGroups from '@/components/GroupManagement/WorkGroups'
// CourseManagement
import CourseManagement from '@/components/CourseManagement'
import ManageCourse from '@/components/CourseManagement/ManageCourse'
import Curriculums from '@/components/CourseManagement/Curriculums'
// System Management
import SystemManagement from '@/components/SystemManagement'
import SystemResources from '@/components/SystemManagement/SystemResources'
import SystemAnnoucements from '@/components/SystemManagement/SystemAnnoucements'
import SystemEmails from '@/components/SystemManagement/SystemEmails'
import EmailSettings from '@/components/SystemManagement/SystemEmails/Settings'
import UserEmails from '@/components/SystemManagement/SystemEmails/UserEmails'
import ManagerEmails from '@/components/SystemManagement/SystemEmails/ManagerEmails'
import ApplicationLogs from '@/components/SystemManagement/ApplicationLogs'
import i18n from '@/components/SystemManagement/i18n'
import i18nFields from '@/components/SystemManagement/i18n/fields'
import ShoppingCart from '@/components/SystemManagement/ShoppingCart'
import ShoppingCartSettings from '@/components/SystemManagement/ShoppingCart/settings'
import ShoppingCartProducts from '@/components/SystemManagement/ShoppingCart/products'
// Edit Role
import EditRole from '@/components/EditRole'
import RoleSettings from '@/components/EditRole/RoleSettings'
import RoleAssignment from '@/components/EditRole/RoleAssignment'
// Edit Workgroup
import EditWorkgroup from '@/components/EditWorkgroup'
import WorkgroupSettings from '@/components/EditWorkgroup/WorkgroupSettings'
import WorkgroupAssignment from '@/components/EditWorkgroup/WorkgroupAssignment'
// Edit Course
import EditCourse from '@/components/EditCourse'
import CourseSettings from '@/components/EditCourse/CourseSettings'
import Certificates from '@/components/EditCourse/Certificates'
import RecurrentTraining from '@/components/EditCourse/RecurrentTraining'
import CourseManagerApproval from '@/components/EditCourse/CourseManagerApproval'
import CourseAssignment from '@/components/EditCourse/CourseAssignment'
import EditCertificates from '@/components/EditCourse/EditCertificates'
// Edit Curriculum
import EditCurriculum from '@/components/EditCurriculum'
import CurriculumSettings from '@/components/EditCurriculum/CurriculumSettings'
import CurriculumCertificates from '@/components/EditCurriculum/Certificates'
import CurriculumRecurrentTraining from '@/components/EditCurriculum/RecurrentTraining'
import CurriculumCourses from '@/components/EditCurriculum/Courses'
import CurriculumAssignment from '@/components/EditCurriculum/CurriculumAssignment'
// Edit User profile
import EditUserProfile from '@/components/EditUserProfile'
import UserInformation from '@/components/EditUserProfile/UserInformation'
import UserProfileSettings from '@/components/EditUserProfile/UserProfileSettings'
import UserContactInfo from '@/components/EditUserProfile/UserContactInfo'
import UserWorkgroup from '@/components/EditUserProfile/UserWorkgroups'
import UserManager from '@/components/EditUserProfile/UserManagerPermissions'
// Reports
import StudentRecords from '@/components/Reports/StudentRecords'
import SummaryReport from '@/components/Reports/SummaryReport'
import RecTrainingReport from '@/components/Reports/RecTrainingReport'
import IndividualReport from '@/components/Reports/Individual'
// My Profile
import MyProfile from '@/components/MyProfile/'
import MyProfileUserInfo from '@/components/MyProfile/UserInformation'
import MyProfileSettings from '@/components/MyProfile/Settings'
import MyProfileContact from '@/components/MyProfile/ContactInformation'
import UserPayment from '@/components/MyProfile/UserPayment'
// My Reports
import MyReports from '@/components/MyReports/'
// Security
import Security from '@/components/Security/'
import LoginCheck from '@/components/Security/LoginCheck'
import CourseIP from '@/components/Security/CourseIP'
// Annoucements
import Annoucements from '@/components/Annoucements'
import PurchaseProduct from '@/components/PurchaseProduct'
import BuyProduct from '@/components/Buy'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/MyCourses',
      name: 'My Courses',
      component: MyCourses,
      alias: '/',
      children: [
        {
          path: 'Resources',
          name: 'Resources',
          component: Resources,
          meta: {
            breadcrumbs: [
              {text: 'My Courses', href: '', active: false},
              {text: 'Resources', href: '', active: true}
            ]
          }
        },
        {
          path: 'Courses',
          name: 'Courses',
          component: Courses,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'My Courses', href: '', active: false},
              {text: 'Courses', href: '', active: true}
            ]
          }
        }
      ],
      meta: {
        navItem: 'My Courses'
      }
    },
    {
      path: '/Reports',
      name: 'Reports',
      component: Reports,
      meta: {
        navItem: 'Reports'
      },
      children: [
        {
          path: 'StudentRecords',
          name: 'Student Records',
          component: StudentRecords,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'Reports', href: '', active: false},
              {text: 'Student Records', href: '', active: true}
            ]
          }
        },
        {
          path: 'SummaryReport',
          name: 'Summary Report',
          component: SummaryReport,
          meta: {
            breadcrumbs: [
              {text: 'Reports', href: '', active: false},
              {text: 'Summary Report', href: '', active: true}
            ]
          }
        },
        {
          path: 'RecTrainingReport',
          name: 'Recurrent Training Report',
          component: RecTrainingReport,
          meta: {
            breadcrumbs: [
              {text: 'Reports', href: '', active: false},
              {text: 'Recurrent Training Report', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/UserProfileManagement',
      name: 'User Profile Management',
      component: UserProfileManagement,
      meta: {
        breadcrumbMagic: 'hidden',
        navItem: 'User Profile Management'
      }
    },
    {
      path: '/ManagerAuthorization',
      name: 'Manager Authorization',
      component: ManagerAuthorization,
      meta: {
        breadcrumbMagic: 'hidden',
        navItem: 'Manager Authorization'
      }
    },
    {
      path: '/GroupManagement',
      name: 'Group Management',
      component: GroupManagement,
      meta: {
        navItem: 'Group Management'
      },
      children: [
        {
          path: 'Roles',
          name: 'Roles',
          component: Roles,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'Group Management', href: '', active: false},
              {text: 'Roles', href: '', active: true}
            ]
          }
        },
        {
          path: 'WorkGroups',
          name: 'Workgroups',
          component: WorkGroups,
          meta: {
            breadcrumbs: [
              {text: 'Group Management', href: '', active: false},
              {text: 'Workgroups', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/IndividualReport',
      name: 'Individual Report',
      component: IndividualReport,
      meta: {
        navItem: 'Reports',
        breadcrumbMagic: 'reports',
        breadcrumbs: [
          {text: 'Reports', href: '', active: false},
          {text: 'Student Records', href: '', active: true},
          {text: 'User', href: '', active: true}
        ]
      }
    },
    {
      path: '/CourseManagement',
      name: 'Add/Update Courses',
      component: CourseManagement,
      children: [
        {
          path: 'ManageCourse',
          name: 'Manage Course',
          component: ManageCourse,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'Course Management', href: '', active: false},
              {text: 'Courses', href: '', active: true}
            ]
          }
        },
        {
          path: 'Curriculums',
          name: 'Curriculums',
          component: Curriculums,
          meta: {
            breadcrumbs: [
              {text: 'Course Management', href: '', active: false},
              {text: 'Curriculums', href: '', active: true}
            ]
          }
        }
      ],
      meta: {
        navItem: 'Course Management'
      }
    },
    {
      path: '/SystemManagement',
      name: 'System Management',
      component: SystemManagement,
      meta: {
        breadcrumbMagic: 'none',
        navItem: 'System Management'
      }
    },
    {
      path: '/NewRole',
      name: 'New Role',
      component: EditRole,
      meta: {
        navItem: 'Add/Update Groups',
        breadcrumbMagic: 'editRole'
      },
      children: [
        {
          path: 'RoleSettings',
          name: 'Settings',
          component: RoleSettings,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'New Role', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'RoleAssignment',
          name: 'Courses Assigned',
          component: RoleAssignment,
          meta: {
            breadcrumbs: [
              {text: 'New Role', href: '', active: false},
              {text: 'Courses Assigned', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/NewWorkgroup',
      name: 'New Workgroup',
      component: EditWorkgroup,
      meta: {
        navItem: 'Add/Update Groups',
        breadcrumbMagic: 'editWorkgroup'
      },
      children: [
        {
          path: 'WorkgroupSettings',
          name: 'Settings',
          component: WorkgroupSettings,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'New Workgroup', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'WorkgroupAssignment',
          name: 'Courses Assigned',
          component: WorkgroupAssignment,
          meta: {
            breadcrumbs: [
              {text: 'New Workgroup', href: '', active: false},
              {text: 'Courses Assigned', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/NewCourse',
      name: 'New Course',
      component: EditCourse,
      meta: {
        breadcrumbMagic: 'course',
        navItem: 'Add/Update Courses'
      },
      children: [
        {
          path: 'CourseSettings',
          name: 'Settings',
          component: CourseSettings,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'test', href: 'wut', active: true},
              {text: 'New Course', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'Certificates',
          name: 'Certificates',
          component: Certificates,
          meta: {
            breadcrumbs: [
              {text: 'New Course', href: '', active: false},
              {text: 'Certificates', href: '', active: true}
            ]
          }
        },
        {
          path: 'RecurrentTraining',
          name: 'Recurrent Training',
          component: RecurrentTraining,
          meta: {
            breadcrumbs: [
              {text: 'New Course', href: '', active: false},
              {text: 'Recurrent Training', href: '', active: true}
            ]
          }
        },
        {
          path: 'CourseManagerApproval',
          name: 'Manager Approval',
          component: CourseManagerApproval,
          meta: {
            breadcrumbs: [
              {text: 'New Course', href: '', active: false},
              {text: 'Manager Approval', href: '', active: true}
            ]
          }
        },
        {
          path: 'CourseAssignment',
          name: 'Assigned to Roles',
          component: CourseAssignment,
          meta: {
            breadcrumbs: [
              {text: 'New Course', href: '', active: false},
              {text: 'Assigned to Roles', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/EditCertificates',
      name: 'Edit Certifcates',
      component: EditCertificates,
      meta: {
        breadcrumbMagic: 'course',
        navItem: 'Course Management',
        breadcrumbs: [
          {text: 'Course Management', href: '', active: false},
          {text: 'Courses', href: '', active: true},
          {text: 'DYNAMIC COURSE NAME', href: '', active: true},
          {text: 'Certifications', href: '', active: true}
        ]
      }
    },
    {
      path: '/NewUserProfile',
      name: 'New User',
      component: EditUserProfile,
      meta: {
        navItem: 'Add/Update Users',
        breadcrumbMagic: 'userprofile'
      },
      children: [
        {
          path: 'UserInformation',
          name: 'User Information',
          component: UserInformation,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'User Information', href: '', active: true}
            ]
          }
        },
        {
          path: 'UserProfileSettings',
          name: 'Settings',
          component: UserProfileSettings,
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'UserContactInfo',
          name: 'Contact Info',
          component: UserContactInfo,
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'Contact Info', href: '', active: true}
            ]
          }
        },
        {
          path: 'UserManager',
          name: 'Manager Permissions',
          component: UserManager,
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'Manager Permissions', href: '', active: true}
            ]
          }
        },
        {
          path: 'UserWorkgroup',
          name: 'Workgroups Managed',
          component: UserWorkgroup,
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'Workgroups Managed', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/NewCurriculum',
      name: 'New Curriculum',
      component: EditCurriculum,
      meta: {
        breadcrumbMagic: 'curriculum',
        navItem: 'Add/Update Courses'
      },
      children: [
        {
          path: 'CurriculumSettings',
          name: 'Settings',
          component: CurriculumSettings,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'New Curriculum', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'CurriculumCertificates',
          name: 'Certificates',
          component: CurriculumCertificates,
          meta: {
            breadcrumbs: [
              {text: 'New Curriculum', href: '', active: false},
              {text: 'Certificates', href: '', active: true}
            ]
          }
        },
        {
          path: 'CurriculumRecurrentTraining',
          name: 'Recurrent Training',
          component: CurriculumRecurrentTraining,
          meta: {
            breadcrumbs: [
              {text: 'New Curriculum', href: '', active: false},
              {text: 'Recurrent Training', href: '', active: true}
            ]
          }
        },
        {
          path: 'CurriculumCourses',
          name: 'Courses',
          component: CurriculumCourses,
          meta: {
            breadcrumbs: [
              {text: 'New Curriculum', href: '', active: false},
              {text: 'Courses', href: '', active: true}
            ]
          }
        },
        {
          path: 'CurriculumAssignment',
          name: 'Assigned To Roles',
          component: CurriculumAssignment,
          meta: {
            breadcrumbs: [
              {text: 'New Curriculum', href: '', active: false},
              {text: 'Assigned To Roles', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/MyProfile',
      name: 'My Profile',
      component: MyProfile,
      alias: '/',
      children: [
        {
          path: 'UserPayment',
          name: 'Payment Information',
          component: UserPayment,
          meta: {
            breadcrumbs: [
              {text: 'New User', href: '', active: false},
              {text: 'User Payment', href: '', active: true}
            ]
          }
        },
        {
          path: 'UserInformation',
          name: 'User Information',
          component: MyProfileUserInfo,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'My Profile', href: '', active: false},
              {text: 'User Information', href: '', active: true}
            ]
          }
        },
        {
          path: 'Settings',
          name: 'Settings',
          component: MyProfileSettings,
          meta: {
            breadcrumbs: [
              {text: 'My Profile', href: '', active: false},
              {text: 'Settings', href: '', active: true}
            ]
          }
        },
        {
          path: 'ContactInformation',
          name: 'Contact Info',
          component: MyProfileContact,
          meta: {
            breadcrumbs: [
              {text: 'My Profile', href: '', active: false},
              {text: 'Contact Info', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/MyReports',
      name: 'My Reports',
      component: MyReports,
      alias: '/'
    },
    {
      path: '/Annoucements',
      name: 'Annoucements',
      component: Annoucements,
      alias: '/'
    },
    {
      path: '/Security',
      name: 'Security',
      component: Security,
      alias: '/',
      meta: {
        navItem: 'System Management'
      },
      children: [
        {
          path: 'LoginCheck',
          name: 'Login ID Check',
          component: LoginCheck,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'System Management', href: '', active: false},
              {text: 'Security', href: '', active: true},
              {text: 'Login ID Check', href: '', active: true}
            ]
          }
        },
        {
          path: 'CourseIpRestriction',
          name: 'Course IP Restriction',
          component: CourseIP,
          meta: {
            breadcrumbs: [
              {text: 'System Management', href: '', active: false},
              {text: 'Security', href: '', active: true},
              {text: 'Course IP Restriction', href: '', active: true}
            ]
          }
        }
      ]
    },
    {
      path: '/SystemResources',
      name: 'System Management Resources',
      component: SystemResources,
      meta: {
        breadcrumbs: [
          {text: 'System Management', href: '', active: false},
          {text: 'Resources', href: '', active: true}
        ],
        navItem: 'System Management'
      }
    },
    {
      path: '/SystemAnnoucements',
      name: 'System Management Annoucements',
      component: SystemAnnoucements,
      meta: {
        breadcrumbs: [
          {text: 'System Management', href: '', active: false},
          {text: 'Annoucements', href: '', active: true}
        ],
        navItem: 'System Management'
      }
    },
    {
      path: '/SystemEmails',
      name: 'System Email',
      component: SystemEmails,
      meta: {
        breadcrumbs: [
          {text: 'System Management', href: '', active: false},
          {text: 'Emails', href: '', active: true}
        ]
      },
      children: [
        {
          path: 'EmailSettings',
          name: 'Settings',
          component: EmailSettings,
          alias: '',
          meta: {
            breadcrumbs: [
              {text: 'System Management', href: '', active: false},
              {text: 'Emails', href: '', active: true},
              {text: 'Settings', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        },
        {
          path: 'UserEmails',
          name: 'User Emails',
          component: UserEmails,
          meta: {
            breadcrumbs: [
              {text: 'System Management', href: '', active: false},
              {text: 'Emails', href: '', active: true},
              {text: 'User Emails', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        },
        {
          path: 'ManagerEmails',
          name: 'Manager Emails',
          component: ManagerEmails,
          meta: {
            breadcrumbs: [
              {text: 'System Management', href: '', active: false},
              {text: 'Emails', href: '', active: true},
              {text: 'Manager Emails', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        }
      ]
    },
    {
      path: '/ApplicationLogs',
      name: 'Application Logs',
      component: ApplicationLogs
    },
    {
      path: '/ManageUserPermission',
      name: 'Manage User Permission',
      component: ManageUserPermission,
      meta: {
        breadcrumbs: [
          {text: 'User Profile Management', href: '', active: false},
          {text: 'DYNAMIC USERNAME', href: '', active: true},
          {text: 'Manager Permissions Presets', href: '', active: true}
        ],
        navItem: 'User Profile Management'
      }
    },
    {
      path: '/i18n',
      name: 'i18n',
      component: i18n,
      children: [
        {
          path: 'Fields',
          name: 'Fields',
          component: i18nFields,
          meta: {
            breadcrumbs: [
              {text: 'i18n', href: '', active: true},
              {text: 'Fields', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        }
      ]
    },
    {
      path: '/ShoppingCart',
      name: 'Shopping Cart',
      component: ShoppingCart,
      children: [
        {
          path: 'Settings',
          name: 'Settings',
          component: ShoppingCartSettings,
          meta: {
            breadcrumbs: [
              {text: 'Shopping Cart', href: '', active: true},
              {text: 'Settings', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        },
        {
          path: 'Products',
          name: 'Products',
          component: ShoppingCartProducts,
          meta: {
            breadcrumbs: [
              {text: 'Shopping Cart', href: '', active: true},
              {text: 'Products', href: '', active: true}
            ],
            navItem: 'System Management'
          }
        }
      ]
    },
    {
      path: '/PurchaseProduct',
      name: 'Buy',
      component: PurchaseProduct,
      meta: {
        breadcrumbs: [
          {text: 'Buy', href: '', active: true},
        ]
      }
    },
    {
      path: '/Buy',
      name: 'Buy',
      component: BuyProduct,
      meta: {
        breadcrumbs: [
          {text: 'Buy', href: '', active: true},
        ]
      }
    }
  ]
})
