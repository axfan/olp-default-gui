var messages = {
  en: {
    role: 'Role | Roles',
    workgroup: 'Workgroup | Workgroups',
    address1: 'Address 1',
    address2: 'Address 2'
  }
}

module.exports = messages
