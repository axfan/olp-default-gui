// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill' // Brute force polyfills
import 'url-search-params-polyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store/index'
import Multiselect from 'vue-multiselect'
import VueI18n from 'vue-i18n'
require('./assets/css/style.scss')

Vue.use(BootstrapVue)
Vue.use(VueI18n)
Vue.component('multiselect', Multiselect)
Vue.config.productionTip = false
Vue.config.devtools = true

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
  beforeCreate () {
    this.$store.commit('getLocalStore')
  }
})
