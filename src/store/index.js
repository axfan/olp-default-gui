import Vue from 'vue'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import axios from 'axios'
import moment from 'moment'
import messages from '@/lang/en'
import colorConvert from 'color-convert'
import $script from 'scriptjs'
import {version} from '../../package.json'

const OLP_URL =  (process.env.NODE_ENV !== 'production') ? 'https://olpdev.insurelearn.com/api' : 'api'

Vue.use(Vuex)
Vue.use(VueI18n)

const state = {
  session: null,
  courses: [],
  curriculums: [],
  curriculum: null,
  course: null,
  workgroups: [],
  workgroup: null,
  roles: [],
  role: null,
  users: [],
  user: null,
  tempUser: null,
  login: null,
  courseRoleId: null,
  myCourses: [],
  individualReport: [],
  checkedItems: [],
  curriculumCourseCheck: [],
  summaryReports: [],
  studentRecords: [],
  possiblePermissions: [1, 2, 4, 8, 4096, 16, 256, 512, 2048, 1024, 32, 128],
  newUserFlag: false,
  feedback: [],
  saveFlag: false,
  tempLogin: null,
  loginFlag: false,
  loadingLogin: false,
  loadingFlag: false,
  forgotPasswordFlag: false,
  totalUsers: null,
  loadedLanguages: ['en'],
  sessionStorage: null,
  companyContact: null,
  contactUs: null,
  company: {primary: '#3567A8', secondary: '#00AC4E'},
  defaultColors: {primary: '#3567A8', secondary: '#00AC4E'},
  style: {},
  i18n: null,
  i18nData: {role: 'Role|Roles', workgroup: 'Workgroup|Workgroups'},
  managerPreset: null,
  individualAnnouncement: null,
  reportRecurrent: [],
  courseResource: null,
  courseCurriculum: null,
  curriculumID: null,
  roleHistory: [],
  workHistory: [],
  studentCount: null,
  summaryCount: null,
  resetPassword: false,
  courseAssigned: [],
  products: [],
  cardFlag: '',
  cardModal: false,
  cartSettings: null,
  config: null,
  paymentCard: null,
  existCustomer: null,
  tempSession: null,
  purchaseFlag: false,
  registrationCart: [],
  publicRole: -1,
  selfRegistraionFlag: false,
  seatInformation: null,
  newUserRoles: [],
  cardError: '',
  errorMessage: '',
  newPurchaseFlag: false,
  userData: '',
  validateLoading: false,
  selfRegistrationLoading: false,
  roleSeats: [],
  tempi18n: '',
  aicc: '',
  events: '',
  system: '',
  olpVersion: version,
  emailActive: false,
  tempUserProfileID: '',
  systemDeleteUser: '',
  loggingout: false,
  firstLoad: true
}

function apiGet (apiPath, params, callback) {
  if (state.login !== null) {
    params['ILSUSER'] = state.login.username
    params['ILSPASSWD'] = state.login.password
  }
  params['format'] = 'json'

  axios.get(OLP_URL + apiPath, {params: params})
    .then(function (result) {
      if (result) callback(result)
    }).then(function (error) {
      console.log(error)
    }).catch(function (error) {
      callback('error')
      console.log(error)
      if (error.response.data.code != null && error.response.data.code == -1) {
      	state.session = null
      }
    })
}
function apiPostPromise (apiPath, params) {
  const axiosConfig = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': '*/*'
    }
  }

  for (var pair of params.entries()) {
    if (pair[1] === 'undefined') {
      params.delete(pair[0])
    }
  }

  if (state.login !== null) {
    params.append('ILSUSER', state.login.username)
    params.append('ILSPASSWD', state.login.password)
    params.append('format', 'json')
  }

  return axios.post(OLP_URL + apiPath, params, axiosConfig)
}

function apiPost (apiPath, params, callback) {
  apiPostPromise(apiPath, params)
    .then(function (result) {
      callback(result)
    }).then(function (error) {
      callback(error)
    }).catch(function (error) {
      callback(error.response)
      if (error.response.data.code != null && error.response.data.code == -1) {
        state.session = null
      }
    })
}

// ==== CONFIG ====
function getConfig () {
  apiGet('/utils/sysconfig/get/', {}, function (result) {
    state.config = result.data.data
  })
}
// ==== Save Changes print ====
function saveChangeSucess () {
  state.feedback = []
  state.feedback.push('Your changes have been saved')
  state.feedback.push(true)
  state.saveFlag = false
}
// ==== Summary Report ====
function getSummaryReports (sort, pageNum, sumBy, search) {
  var params = {
    sumBy: sumBy,
    sort: sort,
    pageNum: pageNum,
    search: search,
    action: 'lookup'
  }
  apiGet('/report/summary/', params, function (result) {
    if (result.data.status === 'success') {
      state.loadingFlag = true
    }
    console.log(result);
    if (result.data.data.summaryItems.count === 0) {
      state.summaryReports = []
    } else {
      state.summaryReports = result.data.data.summaryItems.summary.map((obj, index) => {
        return obj
      })
    }
    sessionStorage.setItem('summaryReports', JSON.stringify(state.summaryReports))
    state.summaryCount = result.data.data.summaryItems.reportCount;
    sessionStorage.setItem('summaryCount', state.summaryCount)
  })
  state.loadingFlag = false
}
// ==== Individual Reports ====
function getIndividualReports (userID) {
  var courseUndefined = false
  var params = {
    userProfileID: userID
  }
  apiGet('/report/individual/', params, function (result) {
    state.loadingFlag = true
    if (result.data.status === 'success') {
      state.individualReport = []
      if (!(result.data.data.report.courses.course instanceof Array)) {
        state.individualReport.push(result.data.data.report.courses.course)
      } else {
        state.individualReport = result.data.data.report.courses.course.map((obj, index) => {
          return obj
        })
      }
      if (result.data.data.report.courses.course === undefined) {
        courseUndefined = true
        state.individualReport = []
      }
      if (result.data.data.report.curricula.curriculum !== undefined) {
        var tempResult = []
        if (!(result.data.data.report.curricula.curriculum instanceof Array)) {
          tempResult.push(result.data.data.report.curricula.curriculum)
        } else {
          tempResult = result.data.data.report.curricula.curriculum
        }
        for (var i = 0; i < tempResult.length; i++) {
          var obj = tempResult[i].courses.course
          for (var j = 0; j < tempResult[i].courses.course.length; j++) {
            obj[j]['currID'] = tempResult[i].courseID
            obj[j]['allowRetake'] = tempResult[i].allowRetake
          }
          var currObj = {
            courseName: tempResult[i].courseName,
            courseNum: tempResult[i].courseNum,
            courseID: tempResult[i].courseID,
            timeFrame: tempResult[i].timeFrame,
            courseAttempts: tempResult[i].courseAttempts,
            allowRetake: tempResult[i].allowRetake,
            curriculumFlag: true
          }
          state.individualReport.push(currObj)
          if (!courseUndefined){
            state.individualReport.splice.apply(state.individualReport, [state.individualReport.findIndex(p => p.courseName === tempResult[i].courseName ) + 1, 0].concat(obj))
          } else {
            for (var k = 0; k < obj.length; k++) {
              state.individualReport.push(obj[k])
            }
          }
        }
      }
      for (var i = 0; i < state.individualReport.length; i++) {
        if (state.individualReport[i].courseAttempts.reportItem !== null && !(state.individualReport[i].courseAttempts.reportItem instanceof Array)) {
          state.individualReport[i].courseAttempts.reportItem = [state.individualReport[i].courseAttempts.reportItem]
        }
      }
    } else {
      state.individualReport = []
    }
  })
  state.loadingFlag = false
}
// ==== Student Records ====
function getStudentReports (status, course, cert, startDate, endDate, hireStart, hireEnd, sort, roles, workgroups, search) {
  var params = {
    status: status,
    courseID: course,
    certification: cert,
    courseStartDate: startDate,
    courseEndDate: endDate,
    hireStartDate: hireStart,
    hireEndDate: hireEnd,
    sort: sort,
    roles: roles,
    workgroups: workgroups,
    search: search,
    pageMax: 15,
    action: 'lookup'
  }
  apiGet('/report/studentrecords/', params, function (result) {
    if (result.data.status === 'success') {
      state.loadingFlag = true
    }
    if (result.data.data.studentRecords.studentRecord === undefined) {
      state.studentRecords = []
      state.studentCount = 0
    } else if (result.data.data.studentRecords.studentRecord.length > 1) {
      state.studentRecords = result.data.data.studentRecords.studentRecord.map((obj, index) => {
        return obj
      })
    } else {
      state.studentRecords = []
      state.studentRecords[0] = result.data.data.studentRecords.studentRecord
    }
    sessionStorage.setItem('studentRecords', JSON.stringify(state.studentRecords))
    state.studentCount = result.data.data.studentRecords.reportCount
    sessionStorage.setItem('studentCount', state.studentCount)
  })
  state.loadingFlag = false
}
// Sort Table
function sortStudentReports (sort, pageNum) {
  var params = {}
  if (sort !== undefined && sort !== '') {
    params['sort'] = sort
  }
  if (pageNum !== undefined && pageNum !== '') {
    params['pageNum'] = pageNum
  }
  apiGet('/report/studentrecords/', params, function (result) {
    state.studentRecords = result.data.data.studentRecords.studentRecord.map((obj, index) => {
      return obj
    })
  })
}

// ==== COURSE =====
function getMyCourses () {
  apiGet('/user/courses/', {}, function (result) {
    if (result.status === 200) {
      state.loadingFlag = true
    }
    state.myCourses = []
    if (result.data.length > 0) {
      var counter = 0
      for (var i = 0; i < result.data.length; i++) {
        if (result.data[i].status === 'Completed') {
          result.data[i].status = 'Complete'
        }
        if (result.data[i].curCourseListings !== undefined) {
          var tempCirriculum = {
            score: result.data[i].score,
            courseName: result.data[i].courseName,
            courseNum: result.data[i].courseNum,
            endDate: result.data[i].endDate,
            certificate: result.data[i].certificate,
            courseID: result.data[i].courseID,
            courseDescription: result.data[i].courseDescription,
            timeFrame: result.data[i].timeFrame,
            startDate: result.data[i].startDate,
            status: result.data[i].status,
            courseType: 'curriculum',
            curriculumIndex: counter
          }
          if (!result.data[i].curriculumCert) {
            tempCirriculum.certificate = null
          }
          state.myCourses.push(tempCirriculum)
          result.data[i].status = 'curriculum'
          for (var j = 0; j < result.data[i].curCourseListings.length; j++) {
            var obj = result.data[i].curCourseListings[j]
            obj['curriculumID'] = i
            if (result.data[i].curCourseListings[j].status === 'Start' || result.data[i].curCourseListings[j].status === 'Re-take') {
              result.data[i].curCourseListings[j].status = 'New'
            }
            if (result.data[i].curriculumCert) {
              obj.certificate = null
            }
            var tempObj = result.data[i].curCourseListings[j]
            tempObj['launch'] = result.data[i].curCourseListings[j].status
            state.myCourses.push(result.data[i].curCourseListings[j])
          }
          counter = counter + 1
          continue
        }
        result.data[i]['launch'] = result.data[i].status
        if (result.data[i].status === 'Start' || result.data[i].status === 'Re-take') {
          result.data[i].status = 'New'
        }
        state.myCourses.push(result.data[i])
        counter = counter + 1
      }
    }
  })
  state.loadingFlag = false
}
function getCourses () {
  apiGet('/course/get/', {courseID: 'ALL'}, function (result) {
    if (result !== 'error') {      
      state.courses = result.data.data.map((obj, index) => {
        return obj.course
      })
    } else {
      state.courses = []
    }
  })
}

function pushCourseRole () {
  var params = {}
  if (state.course.courseID != null) params['courseID'] = state.course.courseID
  params['userData'] = state.course.userData
  params['groupID'] = state.courseRoleId
  params['groupTypeID'] = 1
  params['force'] = true
  apiGet('/group/assigncourse/', params, function (result) {
    return true
  })
}

async function pushCourse () {
  if (state.course.courseNumber === '') {
    state.feedback = []
    state.feedback.push('You must provide a course number')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  if (state.course.courseName.length === 0) {
    state.feedback = []
    state.feedback.push('You must provide a course name')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  if (state.course.courseURL === undefined || state.course.courseURL === '') {
    state.feedback = []
    state.feedback.push('You must select course files')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  var params = new URLSearchParams()
  if (state.course.courseID != null) params.append('courseID', state.course.courseID)
  params.append('courseNumber', state.course.courseNumber)
  params.append('courseName', state.course.courseName)
  params.append('description', state.course.description)
  params.append('courseURL', state.course.courseURL)
  params.append('passScore', state.course.passScore)
  params.append('passExpireDays', state.course.passExpireDays)
  params.append('reassignBeforeDays', state.course.reassignBeforeDays)
  params.append('ipRestrict', state.course.ipRestrict)
  params.append('quickView', state.course.quickView)
  params.append('requiresManagerApproval', state.course.requiresManagerApproval)
  params.append('active', state.course.active)
  params.append('seatTime', state.course.seatTime.minutes)
  params.append('postDate', state.course.postDate)
  params.append('certificateStore', state.course.certificateStore)
  params.append('certificateGenerator', state.course.certificateGenerator)

  apiPost('/course/put/', params, function (result) {
    return true
  })
  if (state.saveFlag) {
    saveChangeSucess()
  }
}

// ==== WORKGROUP ====
function getWorkgroups () {
  var params = {
    groupID: 'ALL',
    groupTypeID: 2
  }

  apiGet('/group/get/', params, function (result) {
    if (result !== 'error') {
      if (result.data.status === 'success') {
        console.log(result)
        if (result.data.data.length > 0) {
          state.workgroups = result.data.data.map((obj, index) => {
            return obj.group
          })
        } else {
          state.workgroups = []
        }
      }
    } else {
      state.workgroups = []
    }
  })
}

function getSingleWorkgroup (groupID) {
  var params = {
    groupID: groupID,
    groupTypeID: 2,
    detail: 'courses'
  }

  apiGet('/group/get/', params, function (result) {
    state.workgroup = state.role = result.data.data[0].group
    getCourses()
    var cleanCourses = []
    if (state.workgroup.courses.count === 1) {
      cleanCourses[0] = state.workgroup.courses.course.courseID
      state.workgroup.courses = cleanCourses
    } else if (state.workgroup.courses.count !== 0) {
      // filter courses from role array
      // Must assign it to a variable or else it becomes undefined
      var courseLength = state.workgroup.courses.course.length
      for (var c = 0; c < courseLength; c++) {
        cleanCourses[c] = state.workgroup.courses.course[c].courseID
        if (c === courseLength - 1) {
          state.workgroup.courses = cleanCourses
        }
      }
    } else {
      // reassigns role courses if count is 0
      state.workgroup.courses = []
    }
  })
}

function getSingleCourse (courseID) {
  var params = {
    courseID: courseID,
    detail: 'extra'
  }

  apiGet('/course/get/', params, function (result) {
    console.log(result.data)
    state.course = result.data.data[0].course
  })
}

async function pushWorkGroupCourse () {
  var params = {}
  params['groupID'] = state.workgroup.groupID
  params['groupTypeID'] = 2
  params['courseID'] = state.workgroup.courseID
  params['forceGroup'] = true
  apiGet('/group/assigncourse/', params, function (result) {
    getWorkgroups()
    return true
  })
  if (state.saveFlag) {
    saveChangeSucess()
  }
}

async function pushGroup () {
  if (state.workgroup.groupName === '') {
    state.feedback = []
    state.feedback.push('You must provide a ' + state.i18n.tc('workgroup', 1) + ' name')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  var params = new URLSearchParams()
  if (state.workgroup.groupID != null) params.append('groupID', state.workgroup.groupID)
  params.append('groupName', state.workgroup.groupName)
  params.append('groupDescription', state.workgroup.groupDescription)
  params.append('groupTypeID', 2)

  apiPost('/group/put/', params, function (result) {
    state.workgroup.groupID = result.data.data.group.groupID
    pushWorkGroupCourse()
    return true
  })
}

// ==== ROLE ====
function getRoles () {
  var params = {
    groupID: 'ALL',
    groupTypeID: 1
  }

  apiGet('/group/get/', params, function (result) {
    if(result !== 'error') {
      if (result.data.status === 'success') {
        if (result.data.data.length > 0) {
          state.roles = result.data.data.map((obj, index) => {
            return obj.group
          })
        } else {
          state.roles = []
        }
      } 
    } else {
      state.roles = []
    }
  })
}

function getSingleRole (groupID) {
  var params = {
    groupID: groupID,
    groupTypeID: 1,
    detail: 'courses'
  }
  apiGet('/group/get/', params, function (result) {
    state.role = result.data.data[0].group
    getCourses()
    var cleanCourses = []
    if (state.role.courses.count === 1) {
      cleanCourses[0] = state.role.courses.course.courseID
      state.role.courses = cleanCourses
    } else if (state.role.courses.count !== 0) {
      // filter courses from role array
      // Must assign it to a variable or else it becomes undefined
      var courseLength = state.role.courses.course.length
      for (var c = 0; c < courseLength; c++) {
        cleanCourses[c] = state.role.courses.course[c].courseID
        if (c === courseLength - 1) {
          state.role.courses = cleanCourses
        }
      }
    } else {
      // reassigns role courses if count is 0
      state.role.courses = []
    }
  })
}

function getManagerPresets (state) {
  var params = {}
  state.managerPreset = null
  apiGet('/securitymode/get/', params, function (result) {
    if (result.data.status === 'success') {
      state.managerPreset = result.data.data
    } else {
      state.managerPreset = []
    }
  })
}

async function pushRole () {
  if (state.role.groupName === '') {
    state.feedback = []
    state.feedback.push('You must provide a ' + state.i18n.tc('role', 1) + ' name')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  var params = new URLSearchParams()
  if (state.role.groupID != null) params.append('groupID', state.role.groupID)
  params.append('groupName', state.role.groupName)
  params.append('groupDescription', state.role.groupDescription)
  params.append('groupTypeID', 1)

  apiPost('/group/put/', params, function (result) {
    state.role.groupID = result.data.data.group.groupID
    pushRoleGroup()
    return true
  })
}

async function pushRoleGroup () {
  var params = {}
  params['groupID'] = state.role.groupID
  params['groupTypeID'] = 1
  params['courseID'] = state.role.courseID
  params['forceGroup'] = true
  apiGet('/group/assigncourse/', params, function (result) {
    getRoles()
    return true
  })
  if (state.saveFlag) {
    saveChangeSucess()
  }
}

// ==== USER ====
function getUsers () {
  var params = {
    action: 'lookup',
    pageMax: 15
  }

  apiGet('/report/users/', params, function (result) {
    state.users = []
    state.loadingFlag = true
    if (result !== 'error') {
      state.totalUsers = result.data.data.userProfiles.userCount
      state.users = []
      if (!(result.data.data.userProfiles.user instanceof Array)) {
        state.users.push(result.data.data.userProfiles.user)
        state.users.map((obj, index) => {
          var tempGroup = null
          if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
            tempGroup = obj.workgroups.group
            delete obj.workgroups.group
            obj.workgroups.group = [tempGroup]
          }
          if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
            tempGroup = obj.roles.group
            delete obj.roles.group
            obj.roles.group = [tempGroup]
          }
          return obj
        })
        sessionStorage.setItem('userData', JSON.stringify(state.users))
      } else {
        state.users = result.data.data.userProfiles.user.map((obj, index) => {
          var tempGroup = null
          if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
            tempGroup = obj.workgroups.group
            delete obj.workgroups.group
            obj.workgroups.group = [tempGroup]
          }
          if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
            tempGroup = obj.roles.group
            delete obj.roles.group
            obj.roles.group = [tempGroup]
          }
          return obj
        })
        sessionStorage.setItem('userData', JSON.stringify(state.users))
      }
    }
    sessionStorage.setItem('userData', JSON.stringify(state.users))
  })
  state.loadingFlag = false
}

function saveManagerPreset (data) {
  if (data.description === '') {
    state.feedback = []
    state.feedback.push('You must enter a preset name')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  if (data.value === 0) {
    state.feedback = []
    state.feedback.push('You must have at least one preset checked')
    state.feedback.push('error')
    state.saveFlag = false
    return
  }
  var params = new URLSearchParams()
  params.append('action', data.action)
  params.append('description', data.description)
  params.append('value', data.value)
  apiPost('/securitymode/put/', params, function (result) {
    return true
  })
  if (state.saveFlag) {
    saveChangeSucess()
  }
}

function sortUserProfileManagement (accessLevel, statusID, workgroupList, roleList, startHire, endHire, search) {
  var params = {
    action: 'lookup',
    roles: roleList,
    workgroups: workgroupList,
    accessLevel: accessLevel,
    status: statusID,
    hireStartDate: startHire,
    hireEndDate: endHire,
    pageMax: 15,
    search: search
  }
  apiGet('/report/users/', params, function (result) {
    if (result.data.status === 'success') {
      state.loadingFlag = true
    }
    console.log(result.data.data.userProfiles)
    sessionStorage.setItem('userTotal', JSON.stringify(result.data.data.userProfiles.userCount))
    state.totalUsers = result.data.data.userProfiles.userCount
    sessionStorage.setItem('userTotal', state.totalUsers)
    state.users = []
    if (state.totalUsers === 0) {
      state.users = []
    } else if (!(result.data.data.userProfiles.user instanceof Array)) {
      state.users.push(result.data.data.userProfiles.user)
      state.users.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
    } else {
      state.users = result.data.data.userProfiles.user.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
    }
    sessionStorage.setItem('userData', JSON.stringify(state.users))
    console.log(state.users)
  })
  state.loadingFlag = false
}
function sortUserReport (sort, pageNum) {
  var params = { pageMax: 15 }
  if (sort !== undefined && sort !== '') {
    params['sort'] = sort
  }
  if (pageNum !== undefined && pageNum !== '') {
    params['pageNum'] = pageNum
  }
  apiGet('/report/users/', params, function (result) {
    if (result.data.status === 'success') {
      state.loadingFlag = true
    }
    state.totalUsers = result.data.data.userProfiles.userCount
    state.users = []
    if (!(result.data.data.userProfiles.user instanceof Array)) {
      state.users.push(result.data.data.userProfiles.user)
      state.users.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
      sessionStorage.setItem('userData', JSON.stringify(state.users))
    } else {
      state.users = result.data.data.userProfiles.user.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
      sessionStorage.setItem('userData', JSON.stringify(state.users))
    }
  })
  state.loadingFlag = false
}

function searchUserReport (search) {
  var params = {
    action: 'lookup',
    search: search,
    pageMax: 15
  }

  apiGet('/report/users/', params, function (result) {
    if (result.data.status === 'success') {
      state.loadingFlag = true
    }
    state.totalUsers = result.data.data.userProfiles.userCount
    state.users = []
    if (!(result.data.data.userProfiles.user instanceof Array)) {
      state.users.push(result.data.data.userProfiles.user)
      state.users.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
    } else {
      state.users = result.data.data.userProfiles.user.map((obj, index) => {
        var tempGroup = null
        if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
          tempGroup = obj.workgroups.group
          delete obj.workgroups.group
          obj.workgroups.group = [tempGroup]
        }
        if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
          tempGroup = obj.roles.group
          delete obj.roles.group
          obj.roles.group = [tempGroup]
        }
        return obj
      })
    }
  })
  state.loadingFlag = false
}

function setI18nLanguage (lang) {
  state.i18n.locale = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

function pushContact (obj) {
  if (obj.name.length === 0) {
    state.feedback = []
    state.feedback.push('You must enter a contact name')
    state.feedback.push('error')
    state.saveFlag = false
  }
  if (obj.email.length === 0) {
    state.feedback = []
    state.feedback.push('You must enter a contact email')
    state.feedback.push('error')
    state.saveFlag = false
  }
  if (obj.phone.length === 0) {
    state.feedback = []
    state.feedback.push('You must enter a contact phone number')
    state.feedback.push('error')
    state.saveFlag = false
  }
  var params = new URLSearchParams()
  params.append('adminContactName', obj.name)
  params.append('adminContactEmail', obj.email)
  params.append('adminContactPhone', obj.phone)
  params.append('companyID', 1)
  var sessionParams = {
    company: {
      adminContactName: obj.name,
      admintContactEmail: obj.email,
      admintContactPhone: obj.phone
    }
  }
  apiPost('/company/put/', params, function (result) {
    return true
  })
  if (state.saveFlag) {
    sessionStorage.setItem('contactUs', JSON.stringify(sessionParams))
    saveChangeSucess()
  }
}
function setTempUserToNull () {
  state.tempLogin = null
}
function changeUserStatusId (users) {
  var params = new URLSearchParams()
  params.append('userProfileID', users.userID)
  params.append('statusID', users.optionValue)
  apiPost('/user/put/', params, function (result) {
  })
}
const mutations = {
  getLocalStore (state) {
    if (localStorage.getItem('store')) {
      this.replaceState(
        Object.assign(state, JSON.parse(localStorage.getItem('store')))
      )
      if (sessionStorage.getItem('userSession') !== null && sessionStorage.getItem('userSession') !== '') {
        var stringSession = sessionStorage.getItem('userSession')
        stringSession = JSON.parse(stringSession)
        state.session = stringSession
      }
      getConfig()
      getWorkgroups()
      getRoles()
      getCourses()
    }
  },
  forgotPasswordToggleTrue (state) {
    state.forgotPasswordFlag = true
  },
  setStudentRecords (item) {
    var convertJSON = sessionStorage.getItem('studentRecords')
    state.studentRecords = JSON.parse(convertJSON)
    state.loadingFlag = false
  },
  setUserData (item) {
    var convertJSON = sessionStorage.getItem('userData')
    state.users = JSON.parse(convertJSON)
    state.loadingFlag = false
  },
  setSummaryData (item) {
    var convertJSON = sessionStorage.getItem('summaryReports')
    state.summaryReports = JSON.parse(convertJSON)
    state.loadingFlag = false
  },
  forgotPasswordToggleFalse (state) {
    state.forgotPasswordFlag = false
  },
  logout (state) {
    state.loggingout = true;
    state.session = null
    state.login = null
    state.loginFlag = false
    state.reportRecurrent = []
    apiGet('/auth/session/close', {}, () => {})
  },
  contact (state, contact) {
    state.saveFlag = true
    pushContact(contact)
  },
  breadcrumbs (state, breadcrumbs) {
    state.breadcrumbs = breadcrumbs
  },
  getCourses (state) {
    getCourses()
  },
  course (state, course) {
    state.course = course
  },
  getRoleCourse (state, roleIds) {
    state.courseRoleId = roleIds
  },
  getWorkgroups (state) {
    getWorkgroups()
  },
  getManagerPresets (state) {
    getManagerPresets(state)
  },
  workgroup (state, workgroup) {
    state.workgroup = workgroup
  },
  getRoles (state) {
    getRoles()
  },
  role (state, role) {
    state.role = role
  },
  pushChecked (state, pushChecked) {
    state.checkedItems = pushChecked
  },
  getUsers (state) {
    getUsers()
  },
  editRole (state, groupID) {
    if (groupID === null) {
      state.role = null
    } else {
      getSingleRole(groupID)
    }
  },
  editWorkgroup (state, groupID) {
    if (groupID === null) {
      state.group = null
    } else {
      getSingleWorkgroup(groupID)
    }
  },
  editCourse (state, courseID) {
    if (courseID === null) {
      state.course = null
    } else {
      getSingleCourse(courseID)
    }
  },
  user (state, user) {
    state.user = user
  },
  newUser (state, flag) {
    state.newUserFlag = flag
  },
  sortUsers (state, obj) {
    getSummaryReports(obj.sort, obj.pageNum, obj.sumBy, obj.search)
  },
  filterStudentRecord (state, obj) {
    getStudentReports(obj.statusID, obj.course, obj.cert, obj.startDate, obj.endDate, obj.hireStart, obj.hireEnd, obj.sort, obj.roles, obj.workgroups, obj.search)
  },
  sortStudentTable (state, obj) {
    sortStudentReports(obj.sort, obj.pageNum)
  },
  sortUserReport (state, obj) {
    sortUserReport(obj.sort, obj.pageNum)
  },
  searchUserReport (state, obj) {
    searchUserReport(obj)
  },
  getMyCourses (state, obj) {
    getMyCourses()
  },
  getUserIndividualReport (state, userID) {
    getIndividualReports(userID)
  },
  sortUserProfileManagement (state, obj) {
    sortUserProfileManagement(obj.accessLevel, obj.statusID, obj.workgroup, obj.role, obj.hsdate, obj.hedate, obj.search)
  },
  changeUserStatus (state, users) {
    changeUserStatusId (users)
  },
  setTempUserToNull (state) {
    setTempUserToNull()
  },
  setContactUs (state, value) {
    state.contactUs = value
    sessionStorage.setItem('contactUs', JSON.stringify(value))
  },
  saveManagerPreset (state, obj) {
    state.saveFlag = true
    saveManagerPreset(obj)
  },
  company (state, value) {
    state.company = value
    let colors = state.company.branding.split('|')
    var primary 
    var secondary
    if (colors.length === 1) {
      primary = state.defaultColors.primary
      secondary = state.defaultColors.secondary
    } else {
      primary = colors[0]
      secondary = colors[1]
    }
    var style = null
    // Color logic
    state.company.primary = primary
    let primaryHSL = colorConvert.hex.hsl(primary)
    primaryHSL[2] += 20
    state.company.primaryHover = colorConvert.hsl.hex(primaryHSL)
    primaryHSL[2] += 20
    state.company.primaryActive = colorConvert.hsl.hex(primaryHSL)
    state.company.secondary = secondary
    let secondaryHSL = colorConvert.hex.hsl(secondary)
    secondaryHSL[2] += 20
    state.company.secondaryHover = colorConvert.hsl.hex(secondaryHSL)
    secondaryHSL[2] -= 40
    state.company.secondaryActive = colorConvert.hsl.hex(secondaryHSL)
    style.backgroundPrimary = { 'background-color': this.$store.state.company.primary }
    style.backgroundSecondary = { 'background-color': this.$store.state.company.secondary, 'border-color': this.$store.state.company.secondary }
    sessionStorage.setItem('company', JSON.stringify(value))
    sessionStorage.setItem('brandingColor', primary + "|" + secondary)
  },
  SET_MANAGER_SEAT : (state, data) => {
    state.seatInformation = data
  },
  SET_SELF_REGISTRATION_FLAG : (state, data) => {
    state.purchaseFlag = data
  },
  SET_USER_SESSION : (state, data) => {
    state.session = data
  },
  SET_CART_SETTINGS : (state, data) => {
    state.cartSettings = data
  },
  SET_CONFIG_SETTINGS : (state, data) => {
    state.config = data
    if (state.config.EmailEnableAssignedRetakes === 'on' || state.config.EmailEnableBeforeDue === 'on'  || state.config.EmailEnableBeforeDue === '1' || state.config.EmailEnableChangePassword === 'on' 
    || state.config.EmailEnableManagerComplReport === 'on' || state.config.EmailEnableManagerRecurReport === 'on' || state.config.EmailEnablePastDue === 'on' ||
    state.config.EmailEnableStudentPassFail === 'on' || state.config.EmailEnableWelcome === 'on' || state.config.EmailEnableRecoverPassUser === 'on' || state.config.EmailEnablePastDue === "1") {
      state.emailActive = true
    } else {
      state.emailActive = false
    }
  },
  SET_CARD_FLAG : (state, data) => {
    console.log(data)
    state.cardFlag = data
  },
  SET_CARD_MODAL : (state, data) => {
    state.cardModal = data
  },
  SET_PAYMENT_CARD : (state, data) => {
    state.paymentCard = data
  },
  SET_ANNOUCEMENT : (state, data) => {
    state.individualAnnouncement = data
  },
  SET_REPORT_RECURRENT : (state, data) => {
    state.reportRecurrent = []
  },
  SET_COURSE_RESOURCE : (state, data) => {
    state.courseResource = data
  },
  SET_CURRICULUM : (state, data) => {
    state.curriculums = data
  },
  SET_SINGLE_CURRICULUM : (state, data) => {
    state.curriculum = data
  },
  SET_CURRICULUM_COURSE : (state, data) => {
    state.courseCurriculum = data
  },
  SET_CURRICULUM_ID : (state, data) => {
    state.curriculumID = data
  },
  SET_CURRICULUM_COURSE_HISTORY : (state, data) => {
    state.curriculumCourseCheck = data
  },
  SET_PUSH_EDIT_WORKGROUPS : (state, data) => {
    state.workHistory = data
  },
  SET_PUBLIC_ROLE : (state, data) => {
    state.publicRole = data
  },
  SET_PUSH_EDIT_ROLES : (state, data) => {
    state.roleHistory = data
  },
  SET_PUSH_NEW_ROLES : (state, data) => {
    state.newUserRoles = data
  },
  SET_RESET_PASSWORD : (state, data) => {
    state.resetPassword = data
  },
  SET_PRODUCTS : (state, data) => {
    state.products = data
  },
  SET_EXISTING_CUSTOMER : (state, data) => {
    state.existCustomer = data
  },
  SET_TEMP_SESSION : (state, data) => {
    state.tempSession = data
  },
  SET_SELF_REGISTRATION_FLAG : (state, data) => {
    state.selfRegistraionFlag = data
  },
  SET_USERS : (state, data) =>{
    state.users = data
  },
  SET_ROLES : (state, data) => {
    state.roles = data
  },
  SET_ROLE_SEAT : (state, data) => {
    state.roleSeats = data
  },
  SET_CARD_ERROR : (state, data) => {
    state.cardError = data
  },
  SET_NEW_PURCHASE_FLAG : (state, data) => {
    state.newPurchaseFlag = data
  },
  SET_NEW_USER : (state, data) => {
    state.user = data
  },
  SET_VALIDATE_LOADING : (state, data) => {
    state.validateLoading = data
  },
  SET_SELF_REGISTER_LOADING : (state, data) => {
    state.selfRegistrationLoading = data
  },
  SET_SHOPPING_CART : (state, data) => {
    state.registrationCart = data
  },
  SET_TEMP_i18n : (state, data) =>{
    state.tempi18n = data
  },
  SET_SECURITY_LOGIN_STATUS : (state, data) => {
    state.config.SecondaryLoginValidationEnable = data
  },
  SET_SECRUITY_LOGIN_IP : (state, data) => {
    state.config.SecondaryLoginValidationIPExceptions = data
  },
  SET_COURSE_SECURITY_STATUS : (state, data) => {
    state.config.IPEnable = data
  },
  SET_COURSE_SECURITY_IP : (state, data) => {
    state.config.IPPatVal = data
  },
  SET_AICC_DATA : (state, data) => {
    state.aicc = data
  },
  SET_SYSTEM_DATA : (state, data) => {
    state.system = data
  },
  SET_EVENTS_DATA : (state, data) => {
    state.events = data
  },
  SET_EMAIL_CONFIG : (state, data) =>{
    state.config[data.variable] = data.value
  },
  SET_EMAIL_ACTIVE : (state, data) =>{
    state.emailActive = data
  },
  SET_WEB_URL : (state, data) => {
    state.config.APICallbackCourseComplete = data
  },
  SET_USER_DELETE_NUMBER : (state, data) => {
    state.systemDeleteUser = data
  },
  SET_WEB_CONTENT_TYPE : (state, data) => {
    state.config.WebhookType = data
  },
  SET_RECURRENT_TRAINING : (state, data) => {
    state.config.AutoAssignRetakes = data
  }
}
const actions = {
  pushCompanySecurity ({state, dispatch}) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('ipEnable', state.config.IPEnable)
    params.append('ipPatVal', state.config.IPPatVal)
    params.append('secondaryLoginValidationIPExceptions', state.config.SecondaryLoginValidationIPExceptions)
    params.append('secondaryLoginValidationEnable', state.config.SecondaryLoginValidationEnable)
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  getSingleUser ({state}, userProfileID) {
    var params = {
      userProfileID: userProfileID,
      detail: 'extra'
    }
  
    apiGet('/user/get/', params, function (result) {
      state.user = result.data.data[0].userProfile
      if (!Array.isArray(state.user.managerGroups.group)) {
        var tempGroup = state.user.managerGroups.group
        delete state.user.managerGroups.group
        state.user.managerGroups.group = [tempGroup]
      }
  
      state.user.workgroupsManaged = (state.user.managerGroups.count > 0) ? state.user.managerGroups.group.map((obj, index) => {
        return obj.groupID
      }) : []
    })
  },
  editUser ({state, dispatch}, userProfileID) {
    if (userProfileID === null) {
      state.user = null
    } else {
      dispatch('getSingleUser', userProfileID)
    }
  },
  course (context) {
    state.saveFlag = true
    pushCourse()
    pushCourseRole()
  },
  workgroup (context) {
    state.saveFlag = true
    pushGroup()
  },
  role (context) {
    state.saveFlag = true
    pushRole()
  },
  user (context) {
    state.saveFlag = true
    // pushUser()
  },
  checkSession () {
    return new Promise((resolve, reject) => {
      if (state.session !== 'undefined' && state.session !== null) {
        resolve(true)
      } else {
        reject(new Error('Username and/or password are incorrect'))
      }
    }, 1000)
  },
  pushCompanySetting ({dispatch}, data) {
    state.saveFlag = true
    if (data.companyName === '') {
      state.feedback = []
      state.feedback.push('You must provide a Company Name')
      state.feedback.push('error')
      state.flag = false
      return
    }
    if (data.companyCopyright === '') {
      state.feedback = []
      state.feedback.push('You must provide a Copyright Text')
      state.feedback.push('error')
      state.flag = false
      return
    }
    var params = new URLSearchParams()
    params.append('companyName', data.companyName)
    params.append('copyright', data.companyCopyright)
    params.append('companyID', 1)
    apiPost('/company/put/', params, function (result) {
      dispatch('getCompany', {companyID: 1})
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  login ({commit, dispatch}, user) {
    state.login = null
    var params = new URLSearchParams()
    params.append('user_name', user.username)
    params.append('passwd', user.password)
    return new Promise((resolve, reject) => {
      apiPostPromise('/auth/session/open/', params).then(function (result) {
        sessionStorage.clear()
        commit('SET_USER_SESSION', result.data.data.session)
        sessionStorage.setItem('userSession', JSON.stringify(result.data.data.session))
        if (process.env.NODE_ENV !== 'production') state.login = user
        getConfig()
        getWorkgroups()
        getRoles()
        getCourses()
        resolve(true)
        dispatch('getCurriculums')
        state.firstLoad = false;
      }).catch(function (error) {
        reject(error)
      })
    })
  },
  resetPassword (context, data) {
    var params = new URLSearchParams()
    params.append('email', data.email)
    return new Promise((resolve, reject) => {
      apiPostPromise('/user/resetpassword/', params).then(function (result) {
        resolve(true)
        sessionStorage.clear()
      }).catch(function (error) {
        reject(error)
      })
    })
  },
  changePassword ({dispatch}, data) {
    var params = new URLSearchParams()
    params.append('s', data.sessionHash)
    return new Promise((resolve, reject) => {
      apiPostPromise('/auth/session/openreset/', params).then(function (result) {
        var params = new URLSearchParams()
        params.append('userProfileID', result.data.data.session.user.internalUserID)
        params.append('passwd', data.password)
        apiPostPromise('/user/put/', params).then(function (result) {
          console.log(result)
          sessionStorage.clear()
          resolve(true)
        })
      }).catch(function (error) {
        reject(error)
      })
    })
  },
  getCompany ({commit, state}, company) {
    var params = {
      companyID: company.companyID
    }
    apiGet('/company/get/', params, function (result) {
      console.log(result.data.data[0].company)
      commit('company', result.data.data[0].company)
    })
  },
  pushCompany ({commit, state}) {
    state.saveFlag = true
    var colors = state.company.branding.split('|')
    if (colors[0] === '') {
      state.feedback = []
      state.feedback.push('The branding is missing a Primary color')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (colors[1] === '') {
      state.feedback = []
      state.feedback.push('The branding is missing a Secondary color')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    var params = new URLSearchParams()
    params.append('companyID', 1)
    params.append('branding', state.company.branding)
    params.append('logoURL', state.company.logoURL)
    apiPost('/company/put/', params, function (result) {
      console.log(result)
      commit('company', result.data.data.company)
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  pushCompanyLogo({commit, state}, file) {
    if (typeof(file.name) != 'undefined') {
      var formData = new FormData()
      formData.append("logo", file)
      axios.post(OLP_URL + '/company/put/?companyID=1', formData, { headers: {'Content-Type': 'multipart/form-data'}})
    }
  },
  pushi18n({commit, state, dispatch}) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('companyID', 1)
    if (state.tempi18n !== '') {
      var roleTemp = state.tempi18n.role + '|' + state.tempi18n.rolePlural
      var workgroupTemp = state.tempi18n.workgroup + '|' + state.tempi18n.workgroupPlural
      state.i18nData.role = roleTemp
      state.i18nData.workgroup = workgroupTemp
      state.i18nData.address1 = state.tempi18n.address1
      state.i18nData.address2 = state.tempi18n.address2
      params.append('role', roleTemp)
      params.append('workgroup', workgroupTemp)
      params.append('address1', state.tempi18n.address1)
      params.append('address2', state.tempi18n.address2)
    } else {
      params.append('role', state.i18nData.role)
      params.append('workgroup', state.i18nData.workgroup)
      params.append('address1', state.i18nData.address1)
      params.append('address2', state.i18nData.address2)
    }
    apiPost('/utils/i18n/put/', params, function (result) {
       dispatch('loadLanguageAsync', 1)
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  getContactSupport ({commit, state}) {
    var params = {
      companyID: 1
    }
    state.contactUs = null
    apiGet('/company/get/', params, function (result) {
      console.log(result)
      commit('setContactUs', result.data.data[0])
    })
  },
  loadLanguageAsync (context, lang) {
     $script(OLP_URL + `/lang/i18n.${lang}.js`, 'i18n');
    setTimeout( function() {
      if (typeof msgs === 'undefined') $script(OLP_URL + `/lang/i18n.js`, 'i18n');
    }, 1000);


    $script.ready(['i18n'], function() {
      state.i18n.setLocaleMessage(lang, msgs)
      state.i18nData = msgs
      console.log(msgs);
      return setI18nLanguage(lang)
    });

    return Promise.resolve(setI18nLanguage(lang))
  },
  getAnnouncements ({commit}, individualAnnouncement) {
    var params = {
      announcementID: 'ALL'
    }
    apiGet('/announcement/get/', params, function (result) {
      var tempArray = []
      if (result !== 'error') {
        if (result.data.data.length > 0) {
          for (var i = 0; i < result.data.data.length; i++) {
            tempArray.push(result.data.data[i].announcement)
          }
        }
      }
      // This commit will call the mutation to set announcement
      commit('SET_ANNOUCEMENT', tempArray)
    }) 
  },
  pushAnnouncement ({commit, dispatch}, obj) {
    state.saveFlag = true
    if (obj.title === '' || obj.title === undefined) {
      state.feedback = []
      state.feedback.push('You must provide an announcement name')
      state.feedback.push('error')
      state.flag = false
      return
    }
    if (obj.message === '' || obj.message === undefined) {
      state.feedback = []
      state.feedback.push('You must provide content')
      state.feedback.push('error')
      state.flag = false
      return
    }
    if (obj.expiration === '' || obj.expiration === '' || obj.expiration === 'Invalid date') {
      state.feedback = []
      state.feedback.push('You must provide an expiration date')
      state.feedback.push('error')
      state.flag = false
      return
    }
    var params = new URLSearchParams()
    params.append('announcementTitle', obj.title)
    params.append('message', obj.message)
    // params.append('postDate', obj.postDate)
    params.append('expiration', obj.expiration)
    if (obj.url != null) params.append('url', obj.url)
    if (obj.announcementID !== undefined) {
      params.append('announcementID', obj.announcementID)
    } else {
      var todayDate = new Date()
      params.append('postDate', moment.utc(todayDate).format('MM/DD/YYYY'))
    }


    apiPost('/announcement/put/', params, function (result) {
      console.log(result.data.data.announcement)
      // Call back that will be called upon success and will call the get announcements action again
      if (result.data.status === 'success') {
        if (obj.file !== undefined) {
          var formData = new FormData()
          formData.append("fileName", obj.file)
          axios.post(OLP_URL + '/announcement/put/?announcementID='+result.data.data.announcement.announceID, formData, { headers: {'Content-Type': 'multipart/form-data'}})
        } else {
          return
        }
        dispatch('getAnnouncements')
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  getCurriculums ({commit}) {
    var params = {
      curriculumID: 'ALL'
    }
    apiGet('/curriculum/get/', params, function (result) {
      if (result !== 'error') {
        if (result.data.status === 'success') {
          if(result.data.data.length > 0) {          
            commit('SET_CURRICULUM', result.data.data)
          } else {
            commit('SET_CURRICULUM', [])
          }
        } 
      } else {
        commit('SET_CURRICULUM', [])
      }
    })
  },
  getCourses () {
    apiGet('/course/get/', {courseID: 'ALL'}, function (result) {
      if (result.data.status === 'success') {
        if (result.data.data.length > 0) {
          state.courses = result.data.data.map((obj, index) => {
            return obj.course
          })
        } else {
          state.courses = []
        }
      } else {
        state.courses = []
      }
    })
  },
  getReportRecurrent ({commit}, obj) {
    var params = {}
    if (obj !== undefined && obj.expire) {
      params['pastDue'] = false
      params['daysTilDue'] = obj.daysTilDue
    }
    apiGet('/report/recurrent/', params, function (result) {
      if (result.data.status === 'success') {
        commit('SET_REPORT_RECURRENT', result.data.data.studentRecords)
      } else {
        commit('SET_REPORT_RECURRENT', [])
      }
    })
  },
  getCourseResource ({commit}) {
    var params = {
      resourceID: 'ALL'
    }
    apiGet('/resource/get/', params, function (result) {
      var tempArray = []
      if (result !== 'error') {
        if (result.data.status === 'success') {
          for (var i = 0; i < result.data.data.length; i++) {
            tempArray.push(result.data.data[i])
          }
        }
      }
      commit('SET_COURSE_RESOURCE', tempArray)
    })
  },
  pushResource ({dispatch}, obj) {
    state.saveFlag = true
    if (obj.title === '') {
      state.feedback = []
      state.feedback.push('You must provide a resource name')
      state.feedback.push('error')
      state.flag = false
      return
    }
    if (obj.expiration === '' || obj.expiration === 'Invalid date') {
      state.feedback = []
      state.feedback.push('You must provide a expiration date')
      state.feedback.push('error')
      state.flag = false
      return
    }
    var params = new URLSearchParams()
    if (obj.resourceID !== undefined) {
      params.append('resourceID', obj.resourceID)
    } else {
      var todayDate = new Date()
      params.append('postDate', moment.utc(todayDate).format('MM/DD/YYYY'))
    }
    if (obj.file !== '') {
      params.append('fileName', obj.file)
    } else {
      if (obj.fileFlag === '') {
        state.feedback = []
        state.feedback.push('You must provide a file')
        state.feedback.push('error')
        state.flag = false
        return
      }
    }
    params.append('resourceTitle', obj.title)
    params.append('message', obj.message)
    params.append('type', obj.type)
    params.append('expiration', obj.expiration)
    if (obj.url != null) params.append('url', obj.url)
    apiPost('/resource/put/', params, function (result) {
      // Call back that will be called upon success and will call the get announcements action again
      console.log(result)
      if (result.data.status === 'success') {
        if (obj.file !== undefined) {
          var formData = new FormData()
          formData.append("fileName", obj.file)
          axios.post(OLP_URL + '/resource/put/?resourceID='+result.data.data.resource.resourceID, formData, { headers: {'Content-Type': 'multipart/form-data'}})
        }
      } else {
        return
      }
      dispatch('getCourseResource')
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  individualReportOverRide ({commit}, obj) {
    var params = new URLSearchParams()
    if (obj.id === 0) {
      params.append('courseAttemptID', -1)
    } else {
      params.append('courseAttemptID', obj.id)
    }
    if (obj.comment.length > 0) {
      params.append('message', obj.comment)
    }
    params.append('courseID', obj.courseID)
    params.append('userProfileID', obj.userID)
    apiPost('/courseattempt/overridestatus/', params, function (result) {
      if (result.data.status === 'success') {
        getIndividualReports(obj.userID)
      }
    })
  },
  individualReportRetake ({commit}, obj) {
    var params = new URLSearchParams()
    params.append('courseAttemptID', obj.id)
    if (obj.comment.length > 0) {
      params.append('reason', obj.comment)
    }
    apiPost('/courseattempt/assignretake/', params, function (result) {
      if (result.data.status === 'success') {
        getIndividualReports(obj.userID)
      }
    })
  },
  getSingleCurriculum ({commit}, curriculumID) {
    if (curriculumID === null) {
      commit('SET_SINGLE_CURRICULUM', null)
    } else {
      var params = {
        curriculumID: curriculumID,
        detail: 'extra',
        format: 'json'
      }
      apiGet('/curriculum/get/', params, function (result) {
        if (result.data.status === 'success') {
          commit('SET_SINGLE_CURRICULUM', result.data.data[0].course)
        }
      })
    }
  },
  pushCourseCurriculum ({state, commit, dispatch}, id) {
    if (id !== null) {
      var courseID
      var sortOrder
      var courseTemp = []
      var sortTemp = []
      for (var i = 0; i < state.courseCurriculum.length; i++) {
        courseTemp.push(state.courseCurriculum[i].courseID)
        if (state.courseCurriculum[i].sortOrder === "") {
          sortTemp.push(0)
        } else {
          sortTemp.push(state.courseCurriculum[i].sortOrder)
        }
      }
      courseID = courseTemp.join('|')
      sortOrder = sortTemp.join('|')
      var params = new URLSearchParams()
      params.append('curriculumID', id)
      params.append('courseID', courseID)
      params.append('sortOrder', sortOrder)
      apiPost('/curriculum/assigncourses/', params, function (result) {
        if (result.data.status !== 'success') {
          dispatch('getCurriculums')
        }
      })
      commit('SET_CURRICULUM_ID', null)
    }
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  loginAsUser ({state}, id) {
    sessionStorage.clear();
    var params = new URLSearchParams()
    params.append('userName', id)
    params.append('format', 'json')
    apiPost('/auth/session/changeuser/', params, function (result) {
      if (result.data.status === 'success') {
        state.session = result.data.data.session
        sessionStorage.setItem('userSession', JSON.stringify(result.data.data.session))
      }
    })
    setTimeout(function(){ location.assign('/') }, 1000)
  },
  getCourses () {
    apiGet('/course/get/', {courseID: 'ALL'}, function (result) {
      if (result !== 'error') {      
        state.courses = result.data.data.map((obj, index) => {
          return obj.course
        })
      } else {
        state.courses = []
      }
    })
  },
  pushCurriculum ({state, commit, dispatch}) {
    state.saveFlag = true
    if (state.curriculum.courseNumber === '' || state.curriculum.courseNumber === null) {
      state.feedback = []
      state.feedback.push('You must provide a curriculum number')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (state.curriculum.courseName.length === 0) {
      state.feedback = []
      state.feedback.push('You must provide a curriculum name')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    var params = new URLSearchParams()
    if (state.curriculum.courseID != null) params.append('curriculumID', state.curriculum.courseID)
    params.append('curriculumNumber', state.curriculum.courseNumber)
    params.append('curriculumName', state.curriculum.courseName)
    params.append('description', state.curriculum.description)
    params.append('passScore', state.curriculum.passScore)
    params.append('passExpireDays', state.curriculum.passExpireDays)
    params.append('reassignBeforeDays', state.curriculum.reassignBeforeDays)
    params.append('ipRestrict', state.curriculum.ipRestrict)
    params.append('quickView', state.curriculum.quickView)
    params.append('requiresManagerApproval', state.curriculum.requiresManagerApproval)
    params.append('active', state.curriculum.active)
    params.append('seatTime', state.curriculum.seatTime.minutes)
    params.append('postDate', moment.utc(state.curriculum.postDate).format('YYYY-MM-DD'))
    params.append('certificateStore', state.curriculum.certificateStore)
    params.append('curriculumCert', state.curriculum.curriculumCert)
    params.append('allowRetake', state.curriculum.allowRetake)
    if (state.curriculum.certificateGenerator === null) {
      params.append('certificateGenerator', '')
    } else {
      params.append('certificateGenerator', state.curriculum.certificateGenerator)
    }
    apiPost('/curriculum/put/', params, function (result) {
      if (result.data.status === 'success') {
        // submit empty
        if (state.courseRoleId === null || state.courseRoleId === '') {
          var params = {}
          console.log(result.data.data.course.courseID)
          if (result.data.data.course.courseID != null) params['courseID'] = result.data.data.course.courseID
          params['userData'] = result.data.data.course.userData
          params['groupID'] = ''
          params['groupTypeID'] = 1
          params['force'] = true
          apiGet('/group/assigncourse/', params, function (result) {
          })
        } else {
          var params = {}
          if (result.data.data.course.courseID != null) params['courseID'] = result.data.data.course.courseID
          params['userData'] = result.data.data.course.userData
          params['groupID'] = state.courseRoleId
          params['groupTypeID'] = 1
          params['force'] = true
          apiGet('/group/assigncourse/', params, function (result) {
            if (result.data.status === 'success') {
            }
          })
        }
        if (state.courseCurriculum.length > 0) {
          commit('SET_CURRICULUM_ID', result.data.data.course.courseID)
          return
        }
      }
    })
    dispatch('getCurriculums')
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  getProducts ({commit}) {
    var params = {
      productID: 'ALL'
    }
    apiGet('/product/get/', params, function (result) {
      var tempArray = []
      console.log(result)
      if (result !== 'error') {
        if (result.data.status === 'success') {
          for (var i = 0; i < result.data.data.length; i++) {
            tempArray.push(result.data.data[i].product)
          }
        }
      }
      commit('SET_PRODUCTS', tempArray)
    })
  },
  pushProducts ({dispatch}, data) {
    state.saveFlag = true
    if (data.name === '') {
      state.feedback = []
      state.feedback.push('You must provide a name')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.price === '') {
      state.feedback = []
      state.feedback.push('You must provide a price')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.groupID === -1 || data.groupID === undefined) {
      state.feedback = []
      state.feedback.push('You must provide a role')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (state.saveFlag) {
      saveChangeSucess()
    }
    console.log(data)
    var params = new URLSearchParams()
    if (data.productID != null) {
      params.append('productID', data.productID)
    }
    params.append('productName', data.name)
    params.append('groupID', data.groupID)
    params.append('price', data.price)
    if (data.description === '') {
      params.append('description', "")
    } else {
      params.append('description', data.description)
    }
    for (let p of params) {
      console.log(p)
    }
    apiPost('/product/put/', params, function (result) {
      if (result.data.status === 'success') {
        dispatch('getProducts')
      }
    })
  },
  getRoles () {
    var params = {
      groupID: 'ALL',
      groupTypeID: 1
    }
  
    apiGet('/group/get/', params, function (result) {
      if(result !== 'error') {
        if (result.data.status === 'success') {
          if (result.data.data.length > 0) {
            state.roles = result.data.data.map((obj, index) => {
              return obj.group
            })
          } else {
            state.roles = []
          }
        } 
      } else {
        state.roles = []
      }
    })
  },
  pushEditCard ({dispatch, commit}, data) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('cardNumber', data.cardNumber)
    params.append('userProfileID', data.userProfileID)
    params.append('expirationDate', data.expirationDate)
    params.append('cardCode', data.cardCode)
    params.append('zip', data.zip)
    apiPost('/authorize.net/customer/put/', params, function (result) {
      console.log('==== ADAM ==== ');
      console.log(result)
      if (result !== 'error') {
        commit('SET_CARD_FLAG', '')
        dispatch('getCard', data.userProfileID)
      } else {
        var obj = {
          errorMessage: result,
          errorFlag: false
        }
        commit('SET_CARD_ERROR', obj)
        state.feedback = []
        state.feedback.push(result.data.message)
        state.feedback.push('error')
        state.saveFlag = false
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  pushCard ({dispatch, commit, state}, data) {
    state.saveFlag = true
    if (data.name === '') {
      state.feedback = []
      state.feedback.push('You must provide a Name')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.cardNumber === '') {
      state.feedback = []
      state.feedback.push('You must provide a Card Number')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.expirationDate === '') {
      state.feedback = []
      state.feedback.push('You must provide a Expiration Date')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.zip === '' || data.zip < 4) {
      state.feedback = []
      state.feedback.push('You must provide a Zip Code')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }

    var params = new URLSearchParams()
    params.append('cardNumber', data.cardNumber)
    params.append('userProfileID', data.userProfileID)
    params.append('expirationDate', data.expirationDate)
    params.append('cardCode', data.cardCode)
    params.append('zip', data.zip)
    params.append('firstName', state.session.user.firstName)
    params.append('lastName', state.session.user.lastName)
    params.append('workgroupName', state.session.workgroups || '')
    params.append('email', state.session.user.userName)
    apiPost('/authorize.net/customer/put/', params, function (result) {
      console.log('=== ADAM PUSH')
      console.log(state.purchaseFlag)
      if (state.purchaseFlag && state.newPurchaseFlag) {
        commit('SET_SELF_REGISTRATION_FLAG', true)
        state.newPurchaseFlag = false
        setTimeout( () => commit('SET_SELF_REGISTRATION_FLAG', false), 1000);
      } else if (result !== undefined && result.data.status === 'success') {
        commit('SET_CARD_FLAG', '')
        commit('SET_CARD_MODAL', false)
        dispatch('getCard', data.userProfileID)
        saveChangeSucess();
      } else {
        console.log(result)
        var obj = {
          errorMessage: result,
          errorFlag: false
        }
        commit('SET_CARD_ERROR', obj)
        state.feedback = []
        state.feedback.push(result.data.message || 'Failed to connect to Authorize.NET')
        state.feedback.push('error')
        state.saveFlag = false
      }
    })
    if (state.purchaseFlag) {
      commit('SET_SELF_REGISTRATION_FLAG', true)
      return
    }
  },
  deleteCard ({dispatch}, data) {
    var params = new URLSearchParams()
    params.append('userProfileID', data.userProfileID)
    apiPost('/authorize.net/customer/delete/', params, function (result) {
      if (result !== 'error') {
        dispatch('getCard', data.userProfileID)
      } else {
        var obj = {
          errorMessage: result,
          errorFlag: false
        }
        commit('SET_CARD_ERROR', obj)
        state.feedback = []
        state.feedback.push(result.data.message || 'Failed to connect to Authorize.NET')
        state.feedback.push('error')
        state.saveFlag = false
      }
    })
  },
  getConfig ({commit}) {
    apiGet('/utils/sysconfig/get/', {}, function (result) {
      console.log(result)
      if (result !== 'error') {
        commit('SET_CONFIG_SETTINGS', result.data.data)
      }
    })
  },
  getCard ({commit}, data) {
    var params = {
      userProfileID: data
    }
    apiGet('/authorize.net/customer/get/', params, function (result) {
      console.log(result)
      if (result !== 'error') {
        if (result.data.status === 'success') {
          commit('SET_PAYMENT_CARD', result.data.data.customerProfile)
          commit('SET_CARD_FLAG', true)
        }  else {
          commit('SET_CARD_FLAG', false)
        }
      } else {
        commit('SET_CARD_FLAG', false)
      }
    })
  },
  pushShoppingCartSettings ({dispatch, state}) {
    var data = state.cartSettings
    var params = new URLSearchParams()
    state.saveFlag = true
    if (data.ShopEnabled) {
      if (data.ShopLogin === '') {
        state.feedback = []
        state.feedback.push('You must provide a Merchant Login ID')
        state.feedback.push('error')
        state.saveFlag = false
        return
      }
      if (data.ShopToken === '') {
        state.feedback = []
        state.feedback.push('You must provide a Merchant Transaction Key')
        state.feedback.push('error')
        state.saveFlag = false
        return
      }
      params.append('shopEnabled', data.ShopEnabled)
      params.append('shopToken', data.ShopToken)
      params.append('shopSandbox', data.ShopSandbox)
      params.append('shopLogin', data.ShopLogin)
    } else {
      params.append('shopEnabled', data.ShopEnabled)
      params.append('shopToken', '')
      params.append('shopTestMode', false)
      params.append('shopLogin', '')
    }
    console.log(data)
    if (data.ShopDefaultRole !== 0) {
      params.append('shopDefaultRole', data.ShopDefaultRole)
      dispatch('assignShoppingCartRole', data.ShopDefaultRole)
    }
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  pushExistingCustomer (val) {
    SET_EXISTING_CUSTOMER('existCustomer', val)
  },
  pushSelfRegister ({commit, dispatch, state}, data) {
    dispatch('getConfig')
    var params = new URLSearchParams()
    params.append('firstName', data.userData.firstName)
    params.append('lastName', data.userData.lastName)
    params.append('passwd', data.userData.password)
    params.append('locationName', data.userData.location)
    params.append('createGroupOnDemand', true)
    params.append('loginID', data.userData.email)
    params.append('statusID', 1)
    params.append('userData', state.userData)
    commit('SET_SHOPPING_CART', data.userData.cart)
    apiPost('/user/register/', params, function (result) {
      if(state.selfRegistrationLoading) {
        if (result !== undefined && result !== 'error') {
          commit('SET_NEW_PURCHASE_FLAG', true)
          commit('SET_SELF_REGISTER_LOADING', false)
          state.purchaseFlag = true
          var resultData = result.data.data.userProfile
          var userID = resultData.userProfileID
          var userLogin = resultData.loginID
          var obj = {}
          obj['userProfileID'] = userID
          obj['loginID'] = userLogin
          obj['passwd'] = data.userData.password
          obj['cardCode'] = data.cardCode
          state.user = userID
          dispatch('openNewUserSession', obj)
        }
      }
    })
  },
  publicAssignRole ({state}) {
    var roleParam = {
      userProfileID: state.user,
      groupTypeID: 1,
    }
    console.log(state.publicRole)
    if (state.publicRole === -1) {
      console.log('i went here and also ' + state.config.ShopDefaultRole)
      roleParam['groupID'] = state.config.ShopDefaultRole
    } else {
      roleParam['groupID'] = state.publicRole
    }
    apiGet('/group/assignuser/', roleParam, function (result) {
      console.log(result)
    })
  },
  assignShoppingCartRole ({}, data) {
    var roleParam = {
      userProfileID: state.session.user.internalUserID,
      groupTypeID: 1,
      groupID: data
    }
    apiGet('/group/assignuser/', roleParam, function (result) {
    })
  },
  openNewUserSession ({dispatch}, data) {
    var params = new URLSearchParams()
    params.append('user_name', data.loginID)
    params.append('passwd', data.passwd)
    var cardCode = data.cardCode
    apiPost('/auth/session/open/', params, function (result) {
      console.log(result)
      if (result !== undefined && result.data.status === 'success') {
        dispatch('pushPublicProduct', cardCode)
      }
    })
  },
  pushPrivateProduct ({dispatch, commit}, data) {
    var product = data.cart.map(e => e.product).join("|")
    var quantity = data.cart.map(e => e.quantity).join('|')
    var params = new URLSearchParams()
    params.append('productID', product)
    params.append('quantity', quantity)
    params.append('cardCode', data.cardCode)
    params.append('userProfileID', state.session.user.internalUserID)
    apiPost('/authorize.net/customer/charge/', params, function (result) {
      if (result.data.status === 'success' || result.status === 'success') {
        var obj = {
          errorFlag: false
        }
        commit('SET_CARD_ERROR', obj)
        dispatch('getManagerSeats')
      } else {
        var obj = {
          errorMessage: result.data.message,
          errorFlag: true
        }
        commit('SET_CARD_ERROR', obj)
      }
    })
  },
  pushPublicProduct ({dispatch, commit, state}, data) {
    var product = state.registrationCart.map(e => e.product).join("|")
    var quantity = state.registrationCart.map(e => e.quantity).join('|')
    var params = new URLSearchParams()
    params.append('productID', product)
    params.append('quantity', quantity)
    params.append('userProfileID', state.user)
    params.append('cardCode', data)
    apiPost('/authorize.net/customer/charge/', params, function (result) {
      if (result !== undefined && result.data.status === 'success') {
        var obj = {
          errorFlag: false
        }
        commit('SET_CARD_ERROR', obj)
        dispatch('publicAssignRole')
      } else {
        var obj = {
          errorMessage: result.data.message,
          errorFlag: true,
          type: 'CHARGE_ERROR'
        }
        commit('SET_NEW_PURCHASE_FLAG', false)
        commit('SET_CARD_ERROR', obj)
      }
    })
  },
  getManagerSeats ({commit}) {
    apiGet('/seat/info/', {}, function (result) {
      if (result !== 'error') {
        commit('SET_MANAGER_SEAT', result.data.data)
      }
    })
  },
  pushUser ({dispatch, state}, data) {
    state.saveFlag = true
    if (data.roles && data.roles.length === 0) {
      state.feedback = []
      state.feedback.push('You must select at least one role')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.workgroups && data.workgroups.length === 0) {
      state.feedback = []
      state.feedback.push('You must select at least one workgroup')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.loginID === '') {
      state.feedback = []
      state.feedback.push('You must provide a username')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.firstName === '') {
      state.feedback = []
      state.feedback.push('You must provide a first name')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.lastName === '') {
      state.feedback = []
      state.feedback.push('You must provide a last name')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (data.passwd === 'password do no match' || data.passwd === 'please confirm password') {
      state.feedback = []
      state.feedback.push('Passwords do not match')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    if (state.tempLogin) {
      var userParam = {
        loginID: data.loginID
      }
      apiGet('/user/get/', userParam, function (result) {
        if (result && result.data && result.data.status === 'success') {
          state.feedback = []
          state.feedback.push('Username is taken')
          state.feedback.push('error')
          state.saveFlag = false
        }
      })
    }
    var params = new URLSearchParams()
    if (data.userProfileID != null) params.append('userProfileID', data.userProfileID)
    if (data.passwd != null) params.append('passwd', data.passwd)
    params.append('userData', data.userData)
    params.append('loginID', data.loginID)
    params.append('firstName', data.firstName)
    params.append('middleName', data.middleName)
    params.append('suffixName', data.suffixName)
    params.append('lastName', data.lastName)
    params.append('statusID', data.statusID)
    params.append('addr1', data.addr1)
    params.append('accessMode', data.access.mode)
    params.append('accessLevel', data.access.level)
    params.append('addr2', data.addr2)
    params.append('city', data.city)
    params.append('state', data.state)
    params.append('zip', data.zip)
    if (data.hireDate != null && data.hireDate instanceof Date && !isNaN(data.hireDate.getTime())) params.append('hireDate', moment.utc(data.hireDate).format('MM/DD/YYYY'))
    if (data.termDate != null && data.termDate instanceof Date && !isNaN(data.termDate.getTime())) params.append('termDate', moment.utc(data.termDate).format('MM/DD/YYYY'))
    params.append('country', data.country)
    params.append('email', data.email)
    params.append('phone', data.phone)
    params.append('ipOverride', data.ipOverride)
    params.append('loginOverride', data.loginOverride)
    params.append('recurrentEmailOverride', data.recurrentEmailOverride)
    apiPost('/user/put/', params, function (result) {
      console.log(result)
      if(result !== undefined && result.data.status === 'success') {
        if(!data.saveNewFlag) {
          state.tempUserProfileID = result.data.data.userProfile
        }
        setTimeout(function(){ dispatch('getUsers') }, 1000);
        
      }
      data.userProfileID = result.data.data.userProfile.userProfileID
      if (state.saveFlag) {
        saveChangeSucess()
      }
  
      // ==== append User workgroups ====
      var workgroupParam = {
        userProfileID: data.userProfileID,
        groupTypeID: 2,
        groupID: data.workgroups.join('|')
      }
      apiGet('/group/assignuser/', workgroupParam, function (result) {
  
      })
      if (state.newUserRoles.length > 0) {
        var newRoleArray = state.newUserRoles.map(function (e) {
          return e.groupID
        })
        data.roles = data.roles.concat(newRoleArray)
      }
      var roleParam = {
        userProfileID: data.userProfileID,
        groupTypeID: 1,
        groupID: data.roles.join('|')
      }
      apiGet('/group/assignuser/', roleParam, function (result) {
        if(result !== undefined && result.data.status === 'success') {
          dispatch('getManagerSeats')
          if (state.config.ShopEnabled || state.config.ShopEnabled === 'true') {
            dispatch('getRoleSeat')
          }
        }
      })
      var managerParam = {
        userProfileID: data.userProfileID,
        groupTypeID: 2,
        groupID: data.workgroupsManaged.join('|')
      }
  
      apiGet('/group/assignmanager/', managerParam, function (result) {
  
      })
      return true
    })
  },
 getUsers ({commit}) {
    var params = {
      action: 'lookup',
      pageMax: 15
    }
  
    apiGet('/report/users/', params, function (result) {
      var users = []
      state.loadingFlag = true
      if (result !== 'error') {
        state.totalUsers = result.data.data.userProfiles.userCount
        users = []
        if (!(result.data.data.userProfiles.user instanceof Array)) {
          users.push(result.data.data.userProfiles.user)
          users.map((obj, index) => {
            var tempGroup = null
            if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
              tempGroup = obj.workgroups.group
              delete obj.workgroups.group
              obj.workgroups.group = [tempGroup]
            }
            if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
              tempGroup = obj.roles.group
              delete obj.roles.group
              obj.roles.group = [tempGroup]
            }
            return obj
          })
          sessionStorage.setItem('userData', JSON.stringify(users))
        } else {
          users = result.data.data.userProfiles.user.map((obj, index) => {
            var tempGroup = null
            if (typeof obj.workgroups.group !== 'undefined' && !Array.isArray(obj.workgroups.group)) {
              tempGroup = obj.workgroups.group
              delete obj.workgroups.group
              obj.workgroups.group = [tempGroup]
            }
            if (typeof obj.roles.group !== 'undefined' && !Array.isArray(obj.roles.group)) {
              tempGroup = obj.roles.group
              delete obj.roles.group
              obj.roles.group = [tempGroup]
            }
            return obj
          })
          sessionStorage.setItem('userData', JSON.stringify(users))
          commit('SET_USERS', users)
        }
      }
      sessionStorage.setItem('userData', JSON.stringify(users))
      commit('SET_USERS', users)
    })
    state.loadingFlag = false
  },
  getRoles ({commit}) {
    var params = {
      groupID: 'ALL',
      groupTypeID: 1
    }
  
    apiGet('/group/get/', params, function (result) {
      if(result !== 'error') {
        if (result.data.status === 'success') {
          if (result.data.data.length > 0) {
            commit('SET_ROLES', result = result.data.data.map((obj, index) => {
              return obj.group
            }))
          } else {
            commit('SET_ROLES', [])
          }
        } 
      } else {
        commit('SET_ROLES', [])
      }
    })
  },
  getRoleSeat ({commit}) {
    var params = {
      groupID: 'ALL',
      groupTypeID: 1,
      seatRoles: true
    }
  
    apiGet('/group/get/', params, function (result) {
      if(result !== 'error') {
        if (result.data.status === 'success') {
          if (result.data.data.length > 0) {
            commit('SET_ROLE_SEAT', result = result.data.data.map((obj, index) => {
              return obj.group
            }))
          } else {
            commit('SET_ROLE_SEAT', [])
          }
        } 
      } else {
        commit('SET_ROLE_SEAT', [])
      }
    })
  },
  validateCard ({commit, dispatch, state}, data) {
    var params = new URLSearchParams()
    params.append('cardNumber', data.cardNum)
    params.append('expirationDate', data.cardExpire)
    params.append('cardCode', data.cardCode)
    params.append('firstName', data.firstName)
    params.append('lastName', data.lastName)
    params.append('workgroupName', data.workgroupName)
    params.append('email', data.email)
    params.append('zip', data.zip)
    apiPost('/authorize.net/customer/put/', params, function (result) {
      console.log('==== ADAM ==== ');
      state.saveFlag = true
      if (state.validateLoading) {
        if (result.data.data === 'Failed to get result' || result.data.result === 'error' || result === 'error') {
          state.feedback = []
          state.feedback.push('There was problem with your credit card information. Please review and try again.')
          state.feedback.push('error')
          state.saveFlag = false
          commit('SET_CARD_ERROR', true)
          return
        } else {
          state.userData = result.data.data
          var obj = {
            cardCode: data.cardCode,
            userData: data.userData
          }
          dispatch('pushSelfRegister', obj)
          commit('SET_SELF_REGISTER_LOADING', true)
        }
        commit('SET_VALIDATE_LOADING', false)
      }
    })
  },
  getApplicationLog ({commit}, dataType) {
    var params = {
      type: dataType
    }
    apiGet('/utils/log/get', params, function (result) {
      if (result !== 'error') {
        if (dataType === 'A') {
          commit('SET_AICC_DATA', result.data)
        } else if (dataType === 'E') {
          commit('SET_EVENTS_DATA', result.data)
        } else if (dataType === 'S') {
          commit('SET_SYSTEM_DATA', result.data)
        }
      }
    })
  },
  pushEmailConfig ({state, dispatch}) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('emailTemplateSubject8', state.config.EmailTemplate8Subject)
    params.append('emailTemplateBody8', state.config.EmailTemplate8Body)
    params.append('emailTemplateSubject7', state.config.EmailTemplate7Subject)
    params.append('emailTemplateBody7', state.config.EmailTemplate7Body)
    params.append('emailTemplateSubject6', state.config.EmailTemplate6Subject)
    params.append('emailTemplateBody6', state.config.EmailTemplate6Body)
    params.append('emailTemplateSubject5', state.config.EmailTemplate5Subject)
    params.append('emailTemplateBody5', state.config.EmailTemplate5Body)
    params.append('emailTemplateSubject4', state.config.EmailTemplate4Subject)
    params.append('emailTemplateBody4', state.config.EmailTemplate4Body)
    params.append('emailTemplateSubject3', state.config.EmailTemplate3Subject)
    params.append('emailTemplateBody3', state.config.EmailTemplate3Body)
    params.append('emailTemplateSubject2', state.config.EmailTemplate2Subject)
    params.append('emailTemplateBody2', state.config.EmailTemplate2Body)
    params.append('emailTemplateSubject1', state.config.EmailTemplate1Subject)
    params.append('emailTemplateBody1', state.config.EmailTemplate1Body)
    params.append('emailTemplateSubject0', state.config.EmailTemplate0Subject)
    params.append('emailTemplateBody0', state.config.EmailTemplate0Body)
    params.append('emailEnableAssignedRetakes', state.config.EmailEnableAssignedRetakes)
    params.append('emailEnableBeforeDue', state.config.EmailEnableBeforeDue)
    params.append('emailEnableChangePassword', state.config.EmailEnableChangePassword)
    params.append('emailEnableManagerComplReport', state.config.EmailEnableManagerComplReport)
    params.append('emailEnableManagerRecurReport', state.config.emailEnableManagerRecurReport)
    params.append('emailEnablePastDue', state.config.EmailEnablePastDue)
    params.append('emailEnableRecoverPassUser', state.config.EmailEnableRecoverPassUser)
    params.append('emailEnableStudentPassFail', state.config.EmailEnableStudentPassFail)
    params.append('emailEnableWelcome', state.config.EmailEnableWelcome)
    params.append('sendMailServer', state.config.SendMailServer)
    params.append('sendMailPort', state.config.SendMailPort)
    params.append('sendMailPassword', state.config.SendMailPasswd)
    params.append('sendMailSig', state.config.SendMailSig)
    params.append('sendMailUser', state.config.SendMailUser)
    params.append('sendMailFrom', state.config.SendMailFrom)
    params.append('siteURL', state.config.SiteURL)
    params.append('emailBeforeDueDate1', state.config.EmailBeforeDueDate1)
    params.append('emailBeforeDueDate2', state.config.EmailBeforeDueDate2)
    params.append('emailPastDueDate1', state.config.EmailPastDueDate1)
    params.append('emailEnableUserEdit', state.config.EmailEnableUserEdit)
    params.append('emailPastDueDate2', state.config.EmailPastDueDate2)
    params.append('complReportNumDays', state.config.ComplReportNumDays)
    params.append('complReportThresholdPercent', state.config.ComplReportThresholdPercent)
    params.append('complReportFreq', state.config.ComplReportFreq)
    params.append('complReportDay', state.config.ComplReportDay)
    params.append('complReportDayOfMonth', state.config.ComplReportDayOfMonth)
    params.append('recurReportFreq', state.config.RecurReportFreq)
    params.append('recurReportNumDays', state.config.RecurReportNumDays)
    params.append('recurReportDayOfMonth', state.config.RecurReportDayOfMonth)
    params.append('recurReportDay', state.config.RecurReportDay)
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== undefined && result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  pushSelfRegistrationSettings ({state, dispatch, commit}, data) {
    state.saveFlag = true
    if (data.key === '' && data.status === 'on') {
      state.feedback = []
      state.feedback.push('You must enter at least one validation key')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    var params = new URLSearchParams()
    params.append('srEnable', data.status)
    params.append('srLengthVal', data.key)
    params.append('srWGVal', data.workgroupVal)
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== undefined && result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  sendSampleWebHook ({state}) {
    state.saveFlag = true
    if (state.config.APICallbackCourseComplete === '') {
      state.feedback = []
      state.feedback.push('Webhook URL is missing. Sample push failed')
      state.feedback.push('error')
      state.saveFlag = false
      return
    }
    var params = {
      courseAttemptID: 1,
      userProfileID: state.session.user.internalUserID,
      type: state.config.WebhookType
    }

    apiGet('/utils/webhook/', params, function (result) {
    })
    if(state.saveFlag) {
      state.feedback = []
      state.feedback.push('Sample push sent to Webhook URL')
      state.feedback.push(true)
      state.saveFlag = false
    }
  },
  pushWebServiceSettings ({state, dispatch}) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('webhookURL', state.config.APICallbackCourseComplete)
    params.append('webhookType', state.config.WebhookType)
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== undefined && result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  systemDeleteUsers ({commit}, data) {
    var params = {}
    if (data.delete) {
      params['action'] = 'delete'
    }
    params['days'] = data.days
    apiGet('/utils/delete/users/', params, function (result) {
      if (result !== null && result !== undefined) {
        if (data.delete) {
          state.feedback = []
          state.feedback.push(state.systemDeleteUser + ' user have been deleted')
          state.feedback.push(true)
          commit('SET_USER_DELETE_NUMBER', '')
        } else {
          commit('SET_USER_DELETE_NUMBER', result.data.data)
        }
      }
    })
  },
  saveRecurrentTraining ({state, dispatch}) {
    state.saveFlag = true
    var params = new URLSearchParams()
    params.append('autoAssignRetakes', state.config.AutoAssignRetakes)
    apiPost('/utils/sysconfig/put/', params, function (result) {
      if (result !== 'error') {
        dispatch('getConfig') 
      }
    })
    if (state.saveFlag) {
      saveChangeSucess()
    }
  },
  deleteExpiredCourse ({state, dispatch}) {
    apiGet('/utils/delete/expiredcourse/', {}, function (result) {
      if (result !== 'error') {
        state.feedback = []
        state.feedback.push(result.data.data + ' course(s) were reset')
        state.feedback.push(true)
        state.saveFlag = false
      }
    })
  }
}
const getters = {
  doneContactSupport: state => {
    return state.contactUs
  }
}

state.i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
})

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})

store.subscribe((mutation, state) => {
  var localState = {
    session: state.session,
    login: state.login
  }
  localStorage.setItem('store', JSON.stringify(localState))
})

export default store
