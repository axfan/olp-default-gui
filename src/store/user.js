export default class User {
  constructor () {
    // Default values
    this.access = {mode: 0, level: 1}
    this.action = null
    this.userProfileID = null
    this.loginID = ''
    this.passwd = null
    this.firstName = ''
    this.middleName = ''
    this.lastName = ''
    this.suffixName = ''
    this.statusID = 1
    this.employeeTitle = ''
    this.addr1 = ''
    this.addr2 = ''
    this.city = ''
    this.state = ''
    this.zip = ''
    this.country = ''
    this.email = ''
    this.phone = ''
    this.hireDate = null
    this.termDate = null
    this.roleName = ''
    this.roleUserData = ''
    this.locationName = ''
    this.locationUserData = ''
    this.createGroupOnDemand = ''
    this.ipOverride = false
    this.recurrentEmailOverride = false
    this.loginOverride = false
  }
}
