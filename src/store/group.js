export default class Group {
  constructor () {
  // Default values
    this.groupID = null
    this.groupName = ''
    this.groupDescription = ''
    this.groupTypeID = 0
    this.userData = ''
    this.detail = ''
  }

  findGroupByID (groups, ids, callback) {
    callback(
      groups.filter(function (obj) {
        return ids.includes(obj.groupID)
      })
    )
  }
}
