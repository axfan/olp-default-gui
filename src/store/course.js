export default class Course {
  constructor () {
    // Default values
    this.courseID = null
    this.courseNumber = ''
    this.userData = ''
    this.courseTypeID = 2
    this.passExpireDays = 0
    this.description = ''
    this.courseURL = ''
    this.passScore = 0
    this.courseName = ''
    this.postDate = new Date()
    this.reassignBeforeDays = 0
    this.ipRestrict = 'F'
    this.seatTime = null
    this.certificateGenerator = ''
    this.checklistURL = null
    this.requiresManagerApproval = false
    this.quickView = false
    this.active = true
    this.seatTime = {minutes: 0}
    this.certificateStore = false
  }
}
